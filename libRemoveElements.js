// ==UserScript==
// @name    libRemoveElements.js
// @match   *://*/*
// @version 1.0
// ==/UserScript==

async function rm_element(element) {
	element.parentNode.removeChild(element);
}

async function rm_elements(...elements) {
	for (let element of elements) {
		rm_element(element);
	}
}
