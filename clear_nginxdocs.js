// ==UserScript==
// @name      Clear Nginx Docs
// @match     *://docs.nginx.com/*
// @version   1.0
// @author    tplasdio at codeberg dot org
// @require   libRemoveElements.js
// @description   17/09/2022, 8:44:10 PM
// ==/UserScript==

const NAME = "Clear Nginx Docs";

async function main() {
	console.log(`USERSCRIPT "${NAME}" BEGIN MAIN`);
	const sidebar = document.querySelector('#sidebar');
	const sidenav = document.querySelector('.sidenav');
	await rm_elements(sidebar, sidenav);
	console.log(`USERSCRIPT "${NAME}" END MAIN`);
}

main();
