# Web-userscripts

A collection of my userscripts that I use for making
navigating the web a bit more bearable.

## Usage

Install the Firemonkey Userscript Manager for your web browser,
and then import or copy the source code of these scripts in them.

_Only Firemonkey will work since I use @require to source scripts
inside other ones, which is Firemonkey specific AFAIK_
