// ==UserScript==
// @name      Clear Stackoverflow
// @match     *://*.stackoverflow.com/*
// @match     *://*.stackexchange.com/*
// @match     *://*.superuser.com/*
// @version   1.0
// @author    tplasdio at codeberg dot org
// @require   libRemoveElements.js
// @description   17/09/2022, 8:42:28 PM
// ==/UserScript==

// See also: https://github.com/jmwoliver/stack_prettifier/blob/master/stack_prettifier.js

const NAME = "Clear Stackoverflow";

async function main() {
	console.log(`USERSCRIPT "${NAME}" BEGIN MAIN`);
	const banner = document.querySelector('.js-consent-banner');
	const top_bar = document.querySelector('.js-top-bar');
	const leftsidebar = document.getElementById("left-sidebar");
	const footer = document.querySelector('.site-footer--container');
	const nosupportbanner = document.querySelector('.bb');  // For IE user-agent
	const signupbanner = document.querySelector('.js-dismissable-hero');
	await rm_elements(banner, top_bar, leftsidebar, footer, nosupportbanner, signupbanner);
	console.log(`USERSCRIPT "${NAME}" END MAIN`);
}

main();
